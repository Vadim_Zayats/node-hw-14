import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Posts } from "./Posts";

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Posts, (post) => post.author)
  posts: Posts[];

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
  createDate: Date;
}
