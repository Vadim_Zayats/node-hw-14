export interface DecodedToken {
  email: string;
  password: string;
}

// frontend

export interface UpdatedData {
  title: string;
  text: string;
  genre: string;
  isPrivate: boolean;
}
